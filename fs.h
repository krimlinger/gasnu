/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>
#include <time.h>

bool is_most_recent_time(const time_t t1[const restrict static 1],
                         const time_t t2[const restrict static 1]);
int get_time(const char f[static 1], time_t t[static 1],
             struct gasnu_error *const err);
int set_last_modified_str_array_time(char **const bufptr,
                                     time_t t[const static 1],
                                     struct gasnu_error *const err);
char *replace_ext(const char str[static 1], const char ext[static 1],
                  struct gasnu_error *const err);
int should_rebuild_object_file(const char src[const static 1],
                               bool should_rebuild[static 1],
                               time_t *const header_time,
                               struct gasnu_error *const err);
int should_rebuild_executable(const char exe[const static 1],
                              char *objs[const static 1],
                              bool should_rebuild[static 1],
                              struct gasnu_error *const err);
