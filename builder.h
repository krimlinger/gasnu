/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

struct builder;
extern const struct builder GCC_BUILDER;

int builder_gen_obj_cmd(const struct builder *const b, char ***bufptr,
                        char src[static 1], char obj[static 1], char **flg);
int builder_gen_exe_cmd(const struct builder *const b, char ***bufptr,
                        char exe[static 1], char *obj[static 1], char **flg,
                        char **lib);
