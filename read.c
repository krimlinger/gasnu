/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <ctype.h>
#include <glob.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "read.h"
#include "util.h"

static void consume_spaces(const char *s[static 1]);
static int glob_errfunc(const char *path, int errno_code);

static void consume_spaces(const char *s[static 1]) {
  while (**s && isspace(**s))
    (*s)++;
}

char *read_word(const char line[static 1]) {
  consume_spaces(&line);
  /* No word found */
  if (!*line)
    return NULL;
  const char *p = line;
  while (*p && !isspace(*p))
    p++;
  return strndup(line, p - line);
}

int read_two_words(const char line[static 1], char **fst_w, char **snd_w) {
  if (!(*fst_w = read_word(line)))
    return -1;
  if (!(*snd_w = read_word(line + strlen(*fst_w) + 1)))
    return -1;
  return 0;
}

static int glob_errfunc(const char *path, int errno_code) { return 0; }

int read_word_list(char ***bufptr, const char line[static 1], bool is_filepath,
                   struct gasnu_error *const err) {
  int ret = 0;
  size_t i, bufsize;
  if (check_ptr_array_alloc((void ***)bufptr, sizeof(char *), &bufsize, &i)) {
    gasnu_error_set_allocation(err);
    return -1;
  }
  char *original_linebuf = strdup(line);
  char *linebuf = original_linebuf;
  glob_t g = {0};
  for (char *w = NULL; (w = strsep(&linebuf, " \n\v\t\r\f"));) {
    /* Skip whitespaces */
    if (*w) {
      size_t glob_matches = 0;
      if (is_filepath) {
        if (glob(w, GLOB_ERR, glob_errfunc, &g))
          printf("warning: unrecognized pattern: \"%s\"\n", w);
        else
          glob_matches += g.gl_pathc;
      }
      if (check_ptr_array_realloc((void ***)bufptr, sizeof(char *), &bufsize,
                                  i + glob_matches + 1)) {
        ret = -1;
        goto free_glob;
      }
      if (glob_matches) {
        for (size_t j = 0; j < glob_matches; i++, j++)
          (*bufptr)[i] = strdup(g.gl_pathv[j]);
      } else {
        (*bufptr)[i] = strdup(w);
        i++;
      }
    }
  }

free_glob:
  (*bufptr)[i] = NULL;
  globfree(&g);
  free(original_linebuf);
  return ret;
}
