/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>

#include "error.h"

char *read_word(const char line[static 1]);
int read_two_words(const char line[static 1], char **fst_w, char **snd_w);
int read_word_list(char ***bufptr, const char line[static 1], bool is_filepath,
                   struct gasnu_error *const err);
void print_str_array(char *const *p);
