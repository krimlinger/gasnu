/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <errno.h>
#include <jansson.h>
#include <stdio.h>
#include <string.h>

#include "error.h"
#include "gasnufile.h"
#include "json.h"
#include "runner.h"
#include "util.h"

#define LOG_FILE "logs.json"

static void json_array_from_str_array(json_t **a, char **bufptr) {
  *a = json_array();
  char **p;
  ARR_FOREACH(bufptr, p) { json_array_append_new(*a, json_string(*p)); }
}

static void json_object_set_str_array(json_t *obj, const char *const key,
                                      char **bufptr) {
  json_t *a = NULL;
  json_array_from_str_array(&a, bufptr);
  json_object_set_new(obj, key, a);
}

char *dump_json_log(const struct gasnu_file *gasnu,
                    const struct runner_logger *const src_logger,
                    const struct runner_logger *const exe_logger,
                    const struct gasnu_error *const err) {
  char *ret = strdup("");
  json_t *r = json_object();
  char *gasnu_error_msg = strdup("");
  if (err && err->kind != NO_ERROR) {
    free(gasnu_error_msg);
    gasnu_error_msg = gasnu_error_to_str(err);
  }
  json_object_set_new(r, "gasnu_error_msg", json_string(gasnu_error_msg));
  free(gasnu_error_msg);
  if (!gasnu)
    goto result;

  json_object_set_str_array(r, "source", gasnu->sources);
  json_object_set_str_array(r, "headers", gasnu->headers);
  json_object_set_str_array(r, "libraries", gasnu->libraries);
  json_object_set_str_array(r, "flags", gasnu->flags);
  json_object_set_new(r, "executable_name", json_string(gasnu->executable));

  json_t *compilation = json_object();
  json_t *commands = json_array();
  json_t *error_msg = json_array();

  int fst_err_i = -1;
  struct gasnu_error **e;
  if (src_logger) {
    ARR_FOREACH(src_logger->errors, e) {
      if ((*e)->kind != NO_ERROR) {
        if (fst_err_i < 0)
          fst_err_i = e - src_logger->errors;
        char *str_err = gasnu_error_to_str(*e);
        json_array_append_new(error_msg, json_string(str_err));
        free(str_err);
      }
    }
  }

  char ***cmds = NULL;
  if (src_logger) {
    ARR_FOREACH(src_logger->commands, cmds) {
      char *c = str_array_to_str(*cmds);
      json_array_append_new(commands, json_string(c));
      free(c);
      if (!(src_logger->continue_on_error) && fst_err_i >= 0 &&
          (cmds - src_logger->commands) >= fst_err_i)
        break;
    }
  }

  json_object_set_new(compilation, "commands", commands);
  json_object_set_new(compilation, "error_msg", error_msg);
  json_object_set_new(r, "compilation", compilation);

  json_t *linking = json_object();
  char *command = strdup("");
  if (exe_logger) {
    free(command);
    command = str_array_to_str(exe_logger->commands[0]);
  }
  char *error_msg2 = strdup("");
  if (exe_logger) {
    ARR_FOREACH(exe_logger->errors, e) {
      if ((*e)->kind != NO_ERROR) {
        free(error_msg2);
        error_msg2 = gasnu_error_to_str(*e);
        break;
      }
    }
  }
  json_object_set_new(linking, "command", json_string(command));
  free(command);
  json_object_set_new(linking, "error_msg", json_string(error_msg2));
  free(error_msg2);
  json_object_set_new(r, "linking", linking);

  json_t *linked_headers = json_array();
  struct linked_header **lh;
  ARR_FOREACH(gasnu->linked_headers, lh) {
    json_t *jlh = json_array();
    json_array_append_new(jlh, json_string((*lh)->header));
    json_array_append_new(jlh, json_string((*lh)->source));
    json_array_append_new(linked_headers, jlh);
  }
  json_object_set_new(r, "linked_headers", linked_headers);

  ret = json_dumps(r, JSON_INDENT(2));

result:
  json_decref(r);
  return ret;
}

int write_log_file(const struct gasnu_file *gasnu,
                   const struct runner_logger *const src_logger,
                   const struct runner_logger *const exe_logger,
                   struct gasnu_error *const err) {
  FILE *fp = fopen(LOG_FILE, "w");
  if (!fp) {
    gasnu_error_set_errno_code(errno, err);
    return -1;
  }
  char *json_dump = dump_json_log(gasnu, src_logger, exe_logger, err);
  // Todo: check for fs write/close ?
  fputs(json_dump, fp);
  fclose(fp);
  free(json_dump);
  return 0;
}
