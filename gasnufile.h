/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>

#include "error.h"

struct linked_header {
  char *header;
  char *source;
};

struct gasnu_file {
    char *executable;
    char **sources;
    char **headers;
    char **libraries;
    char **flags;
    char **objects;
    struct linked_header **linked_headers;
};

void gasnu_file_init(struct gasnu_file *gasnu);
struct gasnu_file *load_gasnu_file(const char *const filename,
                                 struct gasnu_error *const err);
void gasnu_file_destroy(struct gasnu_file *const gasnu);
void gasnu_file_display(struct gasnu_file const *const gasnu);
int gasnu_file_run(const struct gasnu_file *const gasnu, bool continue_on_error,
                  bool dump_json, struct gasnu_error *const err);
