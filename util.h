/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#define ARR_FOREACH(init, s) for ((s) = (init); (s) && (*s); (s++))

#define ARR_SIZE(a, i)                                                         \
  size_t i;                                                                    \
  for (i = 0; (a) && (a[i]); (i++))                                            \
    ;

void print_str_array_with(const char *const msg, char **p);
char *str_array_to_str(char **p);
int check_ptr_array_alloc(void ***bufptr, size_t type_size,
                          size_t bufsize[static 1], size_t index[static 1]);
int check_ptr_array_realloc(void ***bufptr, size_t type_size,
                            size_t bufsize[static 1], size_t new_size);
