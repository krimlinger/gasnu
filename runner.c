/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "error.h"
#include "runner.h"
#include "util.h"

/* Test any kind of realloc fuckery */
#ifndef NDEBUG
#define RUNNER_LOGGER_SIZE 3
#else
#define RUNNER_LOGGER_SIZE 10
#endif

void runner_logger_init(struct runner_logger *logger, bool continue_on_error,
                        size_t size) {
  logger->commands = NULL;
  logger->errors = NULL;
  logger->continue_on_error = continue_on_error;
  logger->size = size;
  logger->index = 0;
  logger->run_index = 0;
}

struct runner_logger *runner_logger_new(bool continue_on_error) {
  struct runner_logger *l = malloc(sizeof(struct runner_logger));
  if (!l)
    return NULL;
  runner_logger_init(l, continue_on_error, RUNNER_LOGGER_SIZE);
  l->commands = calloc(RUNNER_LOGGER_SIZE, sizeof(char **));
  if (!l->commands)
    return NULL;
  l->errors = calloc(RUNNER_LOGGER_SIZE, sizeof(struct gasnu_error *));
  if (!l->errors)
    return NULL;
  return l;
}

void runner_logger_destroy(struct runner_logger *logger) {
  char ***c;
  ARR_FOREACH(logger->commands, c) {
    /* Do not free each string composing a command because each individual
     * string comes from a gasnu_file struct and will be destroyed with
     * the cleanup call to gasnu_file_destroy.
     */
    free(*c);
  }
  free(logger->commands);
  struct gasnu_error **e;
  ARR_FOREACH(logger->errors, e) { free(*e); }
  free(logger->errors);
  free(logger);
}

int runner_logger_append(struct runner_logger *logger, char **command) {
  if (logger->index + 2 >= logger->size) {
    logger->commands =
        realloc(logger->commands, 2 * logger->size * sizeof(char **));
    if (!logger->commands)
      return -1;
    logger->errors = realloc(logger->errors, 2 * logger->size * sizeof(char *));
    if (!logger->errors)
      return -1;
    logger->size *= 2;
  }
  logger->commands[logger->index] = command;
  logger->commands[logger->index + 1] = NULL;
  logger->errors[logger->index + 1] = NULL;
  logger->index++;

  return 0;
}

bool runner_logger_can_run(const struct runner_logger *const logger) {
  return logger->run_index < logger->index;
}

/* Returns -1 if allocation or programming error. */
int runner_logger_run(struct runner_logger *logger) {
  struct gasnu_error *e = gasnu_error_new();
  if (!e) {
    return -1;
  }
  /* Usually a programming error, maybe asset() this ? */
  if (!logger->commands[logger->run_index]) {
    return -1;
  }
  print_str_array_with("running: ", logger->commands[logger->run_index]);
  if (run_cmd(logger->commands[logger->run_index], e))
    gasnu_error_display(e);
  logger->errors[logger->run_index] = e;
  logger->run_index++;
  return 0;
}

int runner_logger_run_all(struct runner_logger *logger) {
  while (runner_logger_can_run(logger)) {
    if (runner_logger_run(logger))
      return -1;
    if (!logger->continue_on_error &&
        logger->errors[logger->run_index - 1]->kind != NO_ERROR)
      return -1;
  }
  return 0;
}

int run_cmd(char *cmd[static 1], struct gasnu_error *const err) {
  pid_t pid = fork();
  /* Having a fork failing or returning before any process is created
   * can deadlock wait() in somes instances. The code should be
   * hardened to fix any possible issues and update err accordingly,
   * depending on the current process. We should probably also check
   * if the process exited normally or was signaled.
   * I'll do that later. Probably. Maybe. If I have the time. One day...
   */
  if (pid < 0) {
    gasnu_error_set_runtime("could not fork process", err);
    return -1;
  } else if (!pid) {
    execvp(*cmd, cmd);
    exit(errno);
  } else {
    int status;
    wait(&status);
    if (WIFEXITED(status)) {
      if (WEXITSTATUS(status)) {
        gasnu_error_set_runtime("subprocess exited with non-zero value", err);
        return -1;
      }
    } else {
      gasnu_error_set_runtime("subprocess did not terminate normally", err);
      return -1;
    }
  }
  return 0;
  /* ...just kidding. Ain't nobody got time for that. */
}
