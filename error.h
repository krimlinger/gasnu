/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#define GASNU_ERROR_TEXT_LENGTH 200

enum gasnu_error_kind {
  NO_ERROR = 0,
  UNDEFINED_ERROR,
  RUNTIME_ERROR,
  GASNU_FILE_FORMAT_ERROR,
  ALLOCATION_ERROR,
  ERRNO_ERROR,
};

struct gasnu_error {
  enum gasnu_error_kind kind;
  /* Use C11 discriminated unions */
  union {
    char text[GASNU_ERROR_TEXT_LENGTH];
    int errno_code;
  };
};

int errno_reset(int err, int prev);
void gasnu_error_init(struct gasnu_error *const err);
struct gasnu_error *gasnu_error_new(void);
void gasnu_error_set_text(const char text[static 1], enum gasnu_error_kind kind,
                         struct gasnu_error *const err);
void gasnu_error_set_runtime(const char text[static 1],
                            struct gasnu_error *const err);
void gasnu_error_set_gasnu_file_format(const char text[static 1],
                                     struct gasnu_error *const err);
void gasnu_error_set_errno_code(int errno_code, struct gasnu_error *const err);
void gasnu_error_set_allocation(struct gasnu_error *const err);
char *gasnu_error_to_str(const struct gasnu_error *const err);
void gasnu_error_display(const struct gasnu_error *const err);
