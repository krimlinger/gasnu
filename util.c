/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

/* Test any kind of realloc fuckery */
#ifndef NDEBUG
#define BUF_SIZE 3
#else
#define BUF_SIZE 10
#endif

static void print_str_array(char **p);

static void print_str_array(char **p) {
  char **s;
  ARR_FOREACH(p, s) { printf(" %s", *s); }
}

void print_str_array_with(const char *const msg, char **p) {
  printf("%s:", msg);
  print_str_array(p);
  puts("");
}

char *str_array_to_str(char **p) {
  if (!p || !*p)
    return strdup("");
  char **s;
  size_t size = 0;
  ARR_FOREACH(p, s) {
    size += strlen((const char *)*s);
    size++;
  }
  char *bufptr = calloc(size, 1);
  if (!bufptr)
    return strdup("");
  size_t i = 0;
  ARR_FOREACH(p, s) {
    strcpy(bufptr + i, *s);
    i += strlen((const char *)*s);
    bufptr[i] = ' ';
    i++;
  }
  bufptr[--i] = '\0';

  return bufptr;
}

/* Allocate a array if necessary and get its allocated size
 * and it's last element's index. */
int check_ptr_array_alloc(void ***bufptr, size_t type_size,
                          size_t bufsize[static 1], size_t index[static 1]) {
  *index = 0;
  *bufsize = BUF_SIZE;
  if (*bufptr) {
    while ((*bufptr)[*index])
      (*index)++;
    *bufsize = *index;
  } else {
    *bufptr = calloc(*bufsize, type_size);
    if (!*bufptr)
      return -1;
  }
  return 0;
}

/* Realloc str array to new_size if necessary */
int check_ptr_array_realloc(void ***bufptr, size_t type_size,
                            size_t bufsize[static 1], size_t new_size) {
  while (new_size >= *bufsize) {
    void *new_buf = realloc(*bufptr, *bufsize * 2 * type_size);
    // void *new_buf = realloc(*bufptr, (*bufsize * 2 + 1) * type_size);
    if (!new_buf)
      return -1;
    *bufptr = new_buf;
    *bufsize *= 2;
  }
  return 0;
}
