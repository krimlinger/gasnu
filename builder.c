/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stddef.h>
#include <stdlib.h>

#include "builder.h"
#include "util.h"

struct builder {
  char *obj_cmd[4];
  char *exe_cmd[3];
};

const struct builder GCC_BUILDER = {{"gcc", "-c", "-o", NULL},
                                    {"gcc", "-o", NULL}};

// Todo: add Java builder.

/* cc -c -o SOURCE.o SOURCE.c CFLAGS */
int builder_gen_obj_cmd(const struct builder *const b, char ***bufptr,
                        char src[static 1], char obj[static 1], char **flg) {
  ARR_SIZE(b->obj_cmd, cmd_size);
  ARR_SIZE(flg, flg_size);
  *bufptr = calloc(cmd_size + flg_size + 3, sizeof(char *));
  if (!*bufptr)
    return -1;
  size_t i;
  for (i = 0; i < cmd_size; i++)
    (*bufptr)[i] = b->obj_cmd[i];
  (*bufptr)[i++] = obj;
  (*bufptr)[i++] = src;
  for (size_t j = 0; j < flg_size; i++, j++)
    (*bufptr)[i] = flg[j];
  (*bufptr)[i] = NULL;

  return 0;
}

/* cc -o EXE SOURCES.o CFLAGS LIBS */
int builder_gen_exe_cmd(const struct builder *const b, char ***bufptr,
                        char exe[static 1], char *obj[static 1], char **flg,
                        char **lib) {
  ARR_SIZE(b->exe_cmd, cmd_size);
  ARR_SIZE(obj, obj_size);
  ARR_SIZE(flg, flg_size);
  ARR_SIZE(lib, lib_size);
  *bufptr =
      calloc(cmd_size + obj_size + flg_size + lib_size + 2, sizeof(char *));
  if (!*bufptr)
    return -1;
  size_t i, j;
  for (i = 0; i < cmd_size; i++)
    (*bufptr)[i] = b->exe_cmd[i];
  (*bufptr)[i++] = exe;
  for (j = 0; j < obj_size; i++, j++)
    (*bufptr)[i] = obj[j];
  for (j = 0; j < flg_size; i++, j++)
    (*bufptr)[i] = flg[j];
  for (j = 0; j < lib_size; i++, j++)
    (*bufptr)[i] = lib[j];
  (*bufptr)[i] = NULL;

  return 0;
}
