/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "builder.h"
#include "error.h"
#include "gasnufile.h"
#include "fs.h"
#include "json.h"
#include "read.h"
#include "runner.h"
#include "util.h"

static int parse_line(struct gasnu_file *const gasnu, const char line[static 1],
                      struct gasnu_error *const err);
static int parse_word_list(char ***bufptr, const char line[static 1],
                           bool is_filepath, struct gasnu_error *const err);
static int parse_executable(struct gasnu_file *const gasnu,
                            const char line[static 1],
                            struct gasnu_error *const err);
static int parse_linked_headers(struct gasnu_file *const gasnu,
                                const char line[static 1],
                                struct gasnu_error *const err);

static int parse_line(struct gasnu_file *const gasnu, const char line[static 1],
                      struct gasnu_error *const err) {
  switch (*line++) {
  case '#':
    break;
  case 'C':
    if (*line == 'H') {
      line++;
      return parse_linked_headers(gasnu, line, err);
    } else
      return parse_word_list(&gasnu->sources, line, true, err);
  case 'H':
    return parse_word_list(&gasnu->headers, line, true, err);
  case 'F':
    return parse_word_list(&gasnu->flags, line, false, err);
  case 'B':
    return parse_word_list(&gasnu->libraries, line, false, err);
  case 'E':
    return parse_executable(gasnu, line, err);
  default:
    /* Ignore line if it is only whitespaces */
    for (line--; *line; line++)
      if (!isspace(*line)) {
        gasnu_error_set_gasnu_file_format("unrecognized gasnufile syntax", err);
        return GASNU_FILE_FORMAT_ERROR;
      }
    break;
  }
  return 0;
}

static int parse_word_list(char ***bufptr, const char line[static 1],
                           bool is_filepath, struct gasnu_error *const err) {
  if (read_word_list(bufptr, line, is_filepath, err))
    return -1;
  return 0;
}

static int parse_executable(struct gasnu_file *const gasnu,
                            const char line[static 1],
                            struct gasnu_error *const err) {
  if (gasnu->executable && *gasnu->executable) {
    gasnu_error_set_gasnu_file_format("executable declared more than once", err);
    return GASNU_FILE_FORMAT_ERROR;
  }
  char *executable = read_word(line);
  if (!executable) {
    gasnu_error_set_gasnu_file_format("invalid executable name", err);
    return GASNU_FILE_FORMAT_ERROR;
  } else {
    gasnu->executable = executable;
  }
  return 0;
}

static int parse_linked_headers(struct gasnu_file *const gasnu,
                                const char line[static 1],
                                struct gasnu_error *const err) {
  char *src, *hd;
  size_t bufsize, i;
  struct linked_header *ld = malloc(sizeof(*ld));
  if (read_two_words(line, &hd, &src) || !ld ||
      check_ptr_array_alloc((void ***)&gasnu->linked_headers, sizeof(*ld),
                            &bufsize, &i)) {
    gasnu_error_set_gasnu_file_format("CH requires exactly two filepaths", err);
    return GASNU_FILE_FORMAT_ERROR;
  }
  check_ptr_array_realloc((void ***)&gasnu->linked_headers, sizeof(*ld),
                          &bufsize, i + 1);
  *ld = (struct linked_header){hd, src};
  gasnu->linked_headers[i] = ld;
  gasnu->linked_headers[i + 1] = NULL;

  return 0;
}

void gasnu_file_init(struct gasnu_file *gasnu) {
  gasnu->executable = NULL;
  gasnu->sources = NULL;
  gasnu->headers = NULL;
  gasnu->libraries = NULL;
  gasnu->flags = NULL;
  gasnu->objects = NULL;
  gasnu->linked_headers = NULL;
}

struct gasnu_file *load_gasnu_file(const char *const filename,
                                 struct gasnu_error *const err) {
  FILE *fp = fopen(filename, "r");
  if (!fp) {
    gasnu_error_set_errno_code(errno, err);
    return NULL;
  }
  struct gasnu_file *gasnu = malloc(sizeof(*gasnu));
  if (!gasnu) {
    gasnu_error_set_allocation(err);
    goto clean_fp;
  }
  gasnu_file_init(gasnu);
  char *line = NULL;
  size_t size = 0;
  int old_errno = errno;
  /* This prevents checking for an old errno
   * but might be not quite right... */
  errno = 0;
  while (1) {
    if (getline(&line, &size, fp) == -1) {
      if (errno) {
        gasnu_error_set_errno_code(errno_reset(errno, old_errno), err);
        goto clean_getline;
      } else
        break;
    }
    if (parse_line(gasnu, line, err) != 0) {
      goto clean_getline;
    }
  }

  free(line);
  fclose(fp);

  ARR_SIZE(gasnu->sources, src_size);
  if (!(gasnu->objects = calloc(++src_size + 1, sizeof(char *)))) {
    gasnu_error_set_allocation(err);
    goto clean_getline;
  }
  char **s;
  ARR_FOREACH(gasnu->sources, s) {
    char *obj = replace_ext(*s, "o", err);
    if (!obj)
      goto clean_getline;
    gasnu->objects[s - gasnu->sources] = obj;
  }
  gasnu->objects[src_size] = NULL;

  return gasnu;

clean_getline:
  free(line);
  gasnu_file_destroy(gasnu);

clean_fp:
  fclose(fp);

  return NULL;
}

void gasnu_file_destroy(struct gasnu_file *const gasnu) {
  if (gasnu->executable)
    free(gasnu->executable);

  char **p;
  if (gasnu->sources) {
    ARR_FOREACH(gasnu->sources, p) { free(*p); }
    free(gasnu->sources);
  }
  if (gasnu->headers) {
    ARR_FOREACH(gasnu->headers, p) { free(*p); }
    free(gasnu->headers);
  }
  if (gasnu->libraries) {
    ARR_FOREACH(gasnu->libraries, p) { free(*p); }
    free(gasnu->libraries);
  }
  if (gasnu->flags) {
    ARR_FOREACH(gasnu->flags, p) { free(*p); }
    free(gasnu->flags);
  }
  if (gasnu->objects) {
    ARR_FOREACH(gasnu->objects, p) { free(*p); }
  }
  free(gasnu->objects);
  struct linked_header **lh;
  if (gasnu->linked_headers) {
    ARR_FOREACH(gasnu->linked_headers, lh) {
      free((*lh)->source);
      free((*lh)->header);
      free(*lh);
    }
    free(gasnu->linked_headers);
  }

  free(gasnu);
}

void gasnu_file_display(struct gasnu_file const *const gasnu) {
  if (gasnu->executable) {
    printf("gasnu executable: %s\n", gasnu->executable);
  }

  if (gasnu->sources)
    print_str_array_with("gasnu sources", gasnu->sources);

  if (gasnu->headers)
    print_str_array_with("gasnu headers", gasnu->headers);

  if (gasnu->libraries)
    print_str_array_with("gasnu libraries", gasnu->libraries);

  if (gasnu->flags)
    print_str_array_with("gasnu flags", gasnu->flags);

  if (gasnu->objects)
    print_str_array_with("gasnu objects", gasnu->objects);

  if (gasnu->linked_headers) {
    size_t i = 0;
    struct linked_header **lh;
    ARR_FOREACH(gasnu->linked_headers, lh) {
      printf("gasnu linked header %zu: %s -> %s\n", i, (*lh)->header,
             (*lh)->source);
      i++;
    }
  }
}

static int gasnu_file_objects_rebuild(const struct gasnu_file *const gasnu,
                                     struct runner_logger *logger,
                                     struct gasnu_error *const err) {
  time_t oldest_unlinked_header;
  if (gasnu->headers)
    if (set_last_modified_str_array_time(gasnu->headers, &oldest_unlinked_header,
                                         err))
      return -1;
  char **s;
  ARR_FOREACH(gasnu->sources, s) {
    time_t oldest_header = oldest_unlinked_header;
    bool should_rebuild = false;
    struct linked_header **lh;
    ARR_FOREACH(gasnu->linked_headers, lh) {
      if (!strcmp((*lh)->source, *s)) {
        time_t lh_time;
        if (get_time((*lh)->header, &lh_time, err))
          return -1;
        if (is_most_recent_time(&lh_time, &oldest_header))
          oldest_header = lh_time;
      }
    }
    if (should_rebuild_object_file(*s, &should_rebuild, &oldest_header, err))
      return -1;
    if (should_rebuild) {
      char **build_cmd = NULL;
      if (builder_gen_obj_cmd(&GCC_BUILDER, &build_cmd, *s,
                              gasnu->objects[s - gasnu->sources], gasnu->flags)) {
        gasnu_error_set_allocation(err);
        return -1;
      }
      if (runner_logger_append(logger, build_cmd)) {
        gasnu_error_set_allocation(err);
        return -1;
      }
    }
  }

  return 0;
}

static int gasnu_file_executable_rebuild(const struct gasnu_file *const gasnu,
                                        struct runner_logger *logger,
                                        struct gasnu_error *const err) {
  bool should_rebuild = false;
  if (gasnu->sources &&
      should_rebuild_executable(gasnu->executable, gasnu->objects,
                                &should_rebuild, err))
    return -1;
  if (should_rebuild) {
    char **build_cmd = NULL;
    if (builder_gen_exe_cmd(&GCC_BUILDER, &build_cmd, gasnu->executable,
                            gasnu->objects, gasnu->flags, gasnu->libraries)) {
      gasnu_error_set_allocation(err);
      return -1;
    }
    runner_logger_append(logger, build_cmd);
  }
  return 0;
}

int gasnu_file_run(const struct gasnu_file *const gasnu, bool continue_on_error,
                  bool dump_json, struct gasnu_error *const err) {
  struct runner_logger *sources_logger = runner_logger_new(continue_on_error),
                       *executable_logger =
                           runner_logger_new(continue_on_error);
  if (!sources_logger || !executable_logger) {
    gasnu_error_set_allocation(err);
    return -1;
  }
  int ret = 0;
  if ((ret = gasnu_file_objects_rebuild(gasnu, sources_logger, err) ||
             runner_logger_run_all(sources_logger)))
    goto free_loggers;
  ret = gasnu_file_executable_rebuild(gasnu, executable_logger, err) ||
        runner_logger_run_all(executable_logger);

free_loggers:
  if (dump_json) {
    char *json_dump =
        dump_json_log(gasnu, sources_logger, executable_logger, err);
    printf("%s\n", json_dump);
    free(json_dump);
  }
  ret = write_log_file(gasnu, sources_logger, executable_logger, err);
  runner_logger_destroy(sources_logger);
  runner_logger_destroy(executable_logger);

  return ret;
}
