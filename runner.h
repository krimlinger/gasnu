/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>

#include "error.h"

struct runner_logger {
  char ***commands;
  struct gasnu_error **errors;
  bool continue_on_error;
  size_t size;
  size_t index;
  size_t run_index;
};

void runner_logger_init(struct runner_logger *logger, bool continue_on_error,
                        size_t size);
struct runner_logger *runner_logger_new(bool continue_on_error);
void runner_logger_destroy(struct runner_logger *logger);
int runner_logger_append(struct runner_logger *logger, char **command);
bool runner_logger_can_run(const struct runner_logger *const logger);
int runner_logger_run(struct runner_logger *logger);
int runner_logger_run_all(struct runner_logger *logger);

int run_cmd(char *cmd[static 1], struct gasnu_error *const err);
