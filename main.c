/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "error.h"
#include "gasnufile.h"
#include "json.h"

int main(int argc, char *argv[argc + 1]) {
  int c, option_index, error_continuation = 0, dump_json = 0;
  struct option long_options[] = {
      {"error-continuation", no_argument, &error_continuation, 1},
      {"dump-json", no_argument, &dump_json, 1},
      {NULL, 0, NULL, 0}};
  while ((c = getopt_long(argc, argv, "dk", long_options, &option_index)) !=
         -1) {
    switch (c) {
    case 'd':
      dump_json = 1;
      break;
    case 'k':
      error_continuation = 1;
      break;
    default:
      break;
    }
  }

  const char *gasnufile = NULL;
  if (optind >= argc) {
    gasnufile = "gasnufile";
  } else {
    gasnufile = argv[optind];
  }

  struct gasnu_error err;
  gasnu_error_init(&err);
  struct gasnu_file *gasnu = load_gasnu_file(gasnufile, &err);
  if (!gasnu) {
    gasnu_error_display(&err);
    if (write_log_file(gasnu, NULL, NULL, &err))
      gasnu_error_display(&err);
    return EXIT_FAILURE;
  }

  if (gasnu_file_run(gasnu, error_continuation, dump_json, &err)) {
    gasnu_error_display(&err);
    gasnu_file_destroy(gasnu);
    return EXIT_FAILURE;
  }

  gasnu_file_destroy(gasnu);

  return EXIT_SUCCESS;
}
