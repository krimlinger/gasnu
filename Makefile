builddir = builddir

all: gasnu

$(builddir):
	meson $(builddir)

$(builddir)/gasnu: $(builddir)
	cd $(builddir) && ninja

gasnu: $(builddir)/gasnu
	cp $(builddir)/gasnu .

clean:
	rm -rf $(builddir)
	rm gasnu
