/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "error.h"
#include "fs.h"
#include "util.h"

bool is_most_recent_time(time_t const t1[restrict static 1],
                         time_t const t2[restrict static 1]) {
  return difftime(*t1, *t2) > 0;
}

int get_time(const char f[const static 1], time_t t[static 1],
             struct gasnu_error *const err) {
  struct stat s;
  if (stat(f, &s)) {
    gasnu_error_set_errno_code(errno, err);
    return -1;
  }
  *t = s.st_mtime;
  return 0;
}

/* Get the most recent time_t in a list of files */
int set_last_modified_str_array_time(char **const bufptr,
                                     time_t t[const static 1],
                                     struct gasnu_error *const err) {
  if (!bufptr) {
    assert("should not call set_last_modified_str_array_time with NULL bufptr");
    return -1;
  }
  time_t tv;
  int c;
  if ((c = get_time(*bufptr, t, err)))
    return c;
  if (!bufptr[1])
    return c;
  char *const *p;
  ARR_FOREACH(bufptr + 1, p) {
    if ((c = get_time(*p, &tv, err)))
      return c;
    if (is_most_recent_time(&tv, t))
      *t = tv;
  }
  return 0;
}

char *replace_ext(const char str[static 1], const char ext[static 1],
                  struct gasnu_error *const err) {

  if (strlen(ext) > 4) {
    gasnu_error_set_runtime("file extension is too long (>4)", err);
    return NULL;
  }
  const char *const ext_ptr = strrchr(str, '.');
  if (!ext_ptr) {
    gasnu_error_set_runtime("cannot determine file extension", err);
    return NULL;
  }
  ptrdiff_t bufsize = ext_ptr - str;
  char *replaced_str = calloc(bufsize + 6, 1);
  if (!replaced_str) {
    gasnu_error_set_allocation(err);
    return NULL;
  }
  memcpy(replaced_str, str, bufsize);
  replaced_str[bufsize] = '.';
  memcpy(replaced_str + bufsize + 1, ext, strlen(ext) + 1);
  return replaced_str;
}

int should_rebuild_object_file(const char src[const static 1],
                               bool should_rebuild[static 1],
                               time_t *const header_time,
                               struct gasnu_error *const err) {
  time_t obj_time, src_time;
  if (get_time(src, &src_time, err))
    return -1;
  char *obj = replace_ext(src, "o", err);
  if (!obj)
    return -1;
  *should_rebuild = false;
  if (get_time(obj, &obj_time, NULL)) {
    *should_rebuild = true;
    goto free_obj;
  }
  if ((header_time && is_most_recent_time(header_time, &obj_time)) ||
      is_most_recent_time(&src_time, &obj_time))
    *should_rebuild = true;
free_obj:
  free(obj);
  return 0;
}

int should_rebuild_executable(const char exe[const static 1],
                              char *objs[const static 1],
                              bool should_rebuild[static 1],
                              struct gasnu_error *const err) {
  time_t exe_time, obj_time;
  if (get_time(exe, &exe_time, err)) {
    *should_rebuild = true;
    return 0;
  }
  if (set_last_modified_str_array_time(objs, &obj_time, err))
    return -1;
  *should_rebuild = is_most_recent_time(&obj_time, &exe_time);
  return 0;
}
