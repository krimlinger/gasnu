# gasnu

## Requirements

To compile this project you will need a modern compiler supporting the C11
standard and the following libraries: GNU's libc with getopt
(GNU's long version),
libjansson and meson. A messy and certainly buggy Makefile is
also provided if that's your jam.

## Building gasnu

Recommended method using meson:

```sh
$ meson builddir
$ cd builddir
$ ninja
$ ./gasnu
```

## Using gasnu

Simply invoke `gasnu` with its build description file as first argument.
If no gasnufile is given, gasnu will try to read in the current directory
a file name `gasnufile`.

```sh
$ gasnu [-c|-d] <gasnufile>
```

After invocation, a JSON `logs.json` file describing the build process will
be generated.

The `-d` flag can be used to dump the JSON log file on stdout at the end
of the build.

The `-c` flag can be used to try to continue the build process if part of it
fails.

## gasnufile syntax

The gasnufile build description syntax is the following:

- `E <file>` name of the output executable
- `C <files>...` list of source files
- `H <files>...` list of header files
- `CH <source> <header>` create an explicit dependency between a source
  and a header file
- `# <comment>` comment line, ignored by gasnu
- `F <flags>...` list of general compilation flags
- `B <flags>...` list of library flags

Lines with `<files>...` arguments should also support simple shell globbing.

## Licensing

The program is release under the GNU GPL version 3 or higher license.
