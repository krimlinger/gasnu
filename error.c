/*
 * Copyright (C) 2019 Ken Rimlinger <ken.rimlinger@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"

inline int errno_reset(int err, int prev) {
  errno = prev;
  return err;
}

void gasnu_error_init(struct gasnu_error *const err) {
  if (err) {
    *err = (struct gasnu_error){.kind = NO_ERROR};
  }
}

struct gasnu_error *gasnu_error_new(void) {
  struct gasnu_error *err = malloc(sizeof(struct gasnu_error));
  if (!err)
    return NULL;
  gasnu_error_init(err);
  return err;
}

void gasnu_error_set_text(const char text[static 1], enum gasnu_error_kind kind,
                         struct gasnu_error *const err) {
  if (err) {
    err->kind = kind;
    snprintf(err->text, GASNU_ERROR_TEXT_LENGTH - 1, "%s", text);
  }
}

void gasnu_error_set_runtime(const char text[static 1],
                            struct gasnu_error *const err) {
  return gasnu_error_set_text(text, RUNTIME_ERROR, err);
}

void gasnu_error_set_gasnu_file_format(const char text[static 1],
                                     struct gasnu_error *const err) {
  return gasnu_error_set_text(text, GASNU_FILE_FORMAT_ERROR, err);
}

void gasnu_error_set_errno_code(int errno_code, struct gasnu_error *const err) {
  if (err) {
    err->kind = ERRNO_ERROR;
    err->errno_code = errno_code;
  }
}

void gasnu_error_set_allocation(struct gasnu_error *const err) {
  if (err) {
    err->kind = ALLOCATION_ERROR;
  }
}

char *gasnu_error_to_str(const struct gasnu_error *const err) {
  char *buf = calloc(GASNU_ERROR_TEXT_LENGTH, 1);
  if (!buf) {
    return NULL;
  }
  switch (err->kind) {
  case NO_ERROR:
    snprintf(buf, GASNU_ERROR_TEXT_LENGTH, "no error happened");
    break;
  case RUNTIME_ERROR:
    snprintf(buf, GASNU_ERROR_TEXT_LENGTH, "a runtime error happened: %.130s",
             err->text);
    break;
  case GASNU_FILE_FORMAT_ERROR:
    snprintf(buf, GASNU_ERROR_TEXT_LENGTH,
             "an error happened while parsing the gasnu file: %.130s",
             err->text);
    break;
  case ALLOCATION_ERROR:
    snprintf(buf, GASNU_ERROR_TEXT_LENGTH,
             "an allocation error happened, please free some memory.");
    break;
  case ERRNO_ERROR:
    snprintf(buf, GASNU_ERROR_TEXT_LENGTH,
             "an errno runtime error with code %.3d happened: %.130s",
             err->errno_code, strerror(err->errno_code));
    break;
  default:
    snprintf(buf, GASNU_ERROR_TEXT_LENGTH,
             "an undefined errror was reported. this should never happen.");
    break;
  }
  return buf;
}

void gasnu_error_display(const struct gasnu_error *const err) {
  char *const e = gasnu_error_to_str(err);
  if (!e)
    fprintf(stderr, "allocation error while displaying error\n");
  printf("%s\n", e);
  free(e);
}
